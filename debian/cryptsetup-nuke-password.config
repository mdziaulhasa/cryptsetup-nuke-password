#!/bin/sh

set -e

. /usr/share/debconf/confmodule || exit 0

if [ -s /etc/cryptsetup-nuke-password/password_hash ]; then
    # What should we do if it's already
    db_input low cryptsetup-nuke-password/already-configured || true
    db_go
fi

# Default value is to overwrite so it's fine to use it even
# if the question has not been asked.
db_get cryptsetup-nuke-password/already-configured
what="$RET"

case "$what" in
    keep|remove)
	# No further questions need to be asked
	exit 0
    ;;
esac

while true; do
    db_input low cryptsetup-nuke-password/password || true
    db_input low cryptsetup-nuke-password/password-again || true
    db_go

    db_get cryptsetup-nuke-password/password || true
    NUKE_PASS="$RET"
    db_get cryptsetup-nuke-password/password-again || true
    NUKE_PASS_CONFIRMATION="$RET"

    if [ "$NUKE_PASS" = "$NUKE_PASS_CONFIRMATION" ]; then
	break
    fi

    db_fset cryptsetup-nuke-password/password-mismatch seen false
    db_fset cryptsetup-nuke-password/password seen false
    db_fset cryptsetup-nuke-password/password-again seen false
    db_set cryptsetup-nuke-password/password ''
    db_set cryptsetup-nuke-password/password-again ''

    db_input critical cryptsetup-nuke-password/password-mismatch || true
    db_go
done
