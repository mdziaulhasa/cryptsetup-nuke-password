CFLAGS += -Wall -O2
LDFLAGS += -lcrypt

EXECUTABLES = crypt

all: $(EXECUTABLES)

check:
	./check

clean:
	rm -f *.o
	rm -f $(EXECUTABLES)

install: $(EXECUTABLES)
	mkdir -p $(DESTDIR)/lib/cryptsetup
	cp askpass $(DESTDIR)/lib/cryptsetup/
	
	mkdir -p $(DESTDIR)/usr/share/initramfs-tools/hooks/
	cp hooks/* $(DESTDIR)/usr/share/initramfs-tools/hooks/
	
	mkdir -p $(DESTDIR)/usr/lib/cryptsetup-nuke-password
	cp crypt $(DESTDIR)/usr/lib/cryptsetup-nuke-password/

.PHONY: check clean install
